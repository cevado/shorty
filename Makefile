SERVICE := shorty
VERSION ?= $(firstword $(shell cat VERSION))

TEST_IMAGE := $(SERVICE):runner
LATEST_IMAGE := $(SERVICE):latest
MAIN_IMAGE := $(SERVICE):$(VERSION)

BUILD_ARGS := --build-arg GIT_COMMIT=$(shell git show -s --format=%H) \
	--build-arg APP_VERSION="$(VERSION)" \
	--build-arg COMMIT_DATE="$(shell git show -s --format=%ci)" \


# TARGETS

.PHONY: help test build credo format dialyzer cleanup local-setup local-deps local-tests local-run

help: ## Print help information for useful tasks
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## Build docker image for this project.
	docker build $(BUILD_ARGS) --cache-from $(LATEST_IMAGE) --tag $(TEST_IMAGE) --target runner .
	docker build $(BUILD_ARGS) --cache-from $(LATEST_IMAGE) --tag $(MAIN_IMAGE) --tag $(LATEST_IMAGE) --target runtime .

credo: ## Runs credo on docker for the application
	docker-compose -p ${SERVICE}_credo run --rm --no-deps --entrypoint "mix credo --strict" $(SERVICE)

format: ## Runs formatter check on docker for the application
	docker-compose -p ${SERVICE}_format run --rm --no-deps --entrypoint "mix format --check-formatted" $(SERVICE)

dialyzer: ## Runs dialyzer on docker for the application
	docker-compose -p ${SERVICE}_dialyzer run --rm --no-deps --entrypoint "mix dialyzer" $(SERVICE)

test: ## Runs tests on docker for the application
	docker-compose -p ${SERVICE}_test run --rm -e MIX_ENV=test --entrypoint "mix test" $(SERVICE)

cleanup: ## Make sure docker-compose environment doesn't leave running containers
	docker-compose down

local-setup: ## Setup local environment to run the application, requires asdf-vm
	asdf install

local-deps: ## Setup mix and fetch dependencies locally
	mix local.hex --force
	mix local.rebar --force
	mix deps.get

local-tests: ## Runs tests locally, requires a postgres database running
	mix test

local-run: ## Runs service locally, requires a postgres database running
	mix phx.server
