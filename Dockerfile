########## VERSIONS ##########
ARG ELIXIR_VERSION=1.13.2
ARG ERLANG_VERSION=24.2
ARG DEBIAN_VERSION=bullseye-20210902
ARG APP_VERSION
ARG APP_NAME=shorty

########## GIT ARGS ##########
ARG COMMIT
ARG COMMIT_DATE
ARG REPO_URL

# ########## DEFINE BASE ##########
FROM hexpm/elixir:${ELIXIR_VERSION}-erlang-${ERLANG_VERSION}-debian-${DEBIAN_VERSION} as base

########## EXPOSING ARGS ON BASE ##########
ARG ELIXIR_VERSION
ARG ERLANG_VERSION
ARG DEBIAN_VERSION

########## SETTING WORKING ENV FOR BASE ##########
WORKDIR /build

RUN apt-get update && \
    apt-get install --no-install-recommends -y git && \
    apt-get clean && \
    rm -rf /var/cache/apt/*

RUN mix do local.hex --force, local.rebar --force

########## CACHING CONFIG FOR DEPS COMPILATION ##########
COPY mix.exs VERSION ./
COPY config/config.exs config/prod.exs config/test.exs config/dev.exs config/

RUN mix deps.get && \
    MIX_ENV=dev mix deps.compile && \
    MIX_ENV=test mix deps.compile && \
    MIX_ENV=prod mix deps.compile

########## COPY PROJECT ##########
COPY .formatter.exs .credo.exs ./
COPY config/runtime.exs config/config.exs config/test.exs config/prod.exs config/
COPY lib lib
COPY rel rel
COPY priv priv
COPY test test

########## DEFINE RUNNER ##########
FROM base as runner

RUN mix compile && \
    MIX_ENV=dev mix phx.digest && \
    MIX_ENV=prod mix phx.digest

WORKDIR /build

##########  DEFINE RELEASE ##########
FROM base as release

ENV MIX_ENV prod
WORKDIR /build

RUN mix release

########## DEFINE RUNTIME ##########
FROM debian:${DEBIAN_VERSION}-slim as runtime

ENV PORT 4000
EXPOSE $PORT

CMD ["/app/start.sh"]

########## INSTALL RUNTIME DEPENDENCIES AND SET PROPER LOCALE ##########
RUN apt-get update && \
    apt-get install --no-install-recommends -y openssl locales && \
    apt-get clean && \
    rm -rf /var/cache/apt/*

RUN sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen && \
    locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

########## PREPARE RUNTIME ##########
WORKDIR /app

RUN groupadd app
COPY --from=release --chown=nobody:app /build/_build/prod/rel/${APP_NAME} ./
COPY --chown=nobody:app bin/start.sh ./

LABEL GIT_COMMIT="$COMMIT" \
      GIT_COMMIT_DATE="$COMMIT_DATE" \
      APP_VERSION="$APP_VERSION"
