defmodule Core.Repo.Migrations.CreateUrls do
  use Ecto.Migration

  def change do
    create table("urls") do
      add :shortcode, :text, null: false
      add :path, :text, null: false

      timestamps(type: :naive_datetime_usec)
    end

    create unique_index("urls", [:shortcode])
    create unique_index("urls", [:path])
  end
end
