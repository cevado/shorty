defmodule Web.Controllers.PageTest do
  use Web.ConnCase, async: true

  describe "GET /" do
    test "landing page", %{conn: conn} do
      conn = get(conn, "/")
      assert html_response(conn, 200) =~ "Welcome to Shorty."
    end
  end

  describe "POST /" do
    test "successfull shortcode", %{conn: conn} do
      response =
        conn
        |> post("/", %{"path" => "https://some.valid.url"})
        |> html_response(201)

      assert response =~ "Copy this url:"
    end

    test "not an url", %{conn: conn} do
      response =
        conn
        |> post("/", %{"path" => "not an uri"})
        |> html_response(400)

      assert response =~ "You must provide a valid url."
    end

    test "must include scheme", %{conn: conn} do
      response =
        conn
        |> post("/", %{"path" => "can.be.uri"})
        |> html_response(400)

      assert response =~ "The url must include the http(s) part."
    end
  end

  describe "GET /:shortcode" do
    test "successfull redirect", %{conn: conn} do
      shortcode = Core.ShortUrl.create(%{"path" => "https://some.valid.url"})

      response =
        conn
        |> get("/#{shortcode}")
        |> html_response(302)

      assert response =~ "redirected"
    end

    test "invalid shortcode", %{conn: conn} do
      response =
        conn
        |> get("/not-a-short-code")
        |> html_response(404)

      assert response =~ "Invalid short url!"
    end
  end
end
