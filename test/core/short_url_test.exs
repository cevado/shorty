defmodule Core.ShortUrlTest do
  use Core.DataCase, async: true

  describe "create/1" do
    test "create shortcode" do
      shortcode = Core.ShortUrl.create(%{"path" => "https://can.be.url"})

      assert is_binary(shortcode)
    end

    test "return existing shortcode" do
      url = "https://can.be.url"
      shortcode = Core.ShortUrl.create(%{"path" => url})

      assert Core.ShortUrl.create(%{"path" => url}) == shortcode
    end

    test "not http" do
      assert Core.ShortUrl.create(%{"path" => "can.be.url"}) == :not_http
    end

    test "invalid url" do
      assert Core.ShortUrl.create(%{"path" => "not a url"}) == :invalid_url
    end
  end

  describe "find/1" do
    test "not found" do
      refute Core.ShortUrl.find("not-a-short-code")
    end

    test "shortcode found" do
      url = "https://can.be.url"
      shortcode = Core.ShortUrl.create(%{"path" => url})
      ConCache.delete(:shortcode_cache, shortcode)

      assert Core.ShortUrl.find(shortcode) == url
    end

    test "cached shortcode" do
      Cache.Shortcode.put("not-a-real-shortcode", "something cached")

      assert Core.ShortUrl.find("not-a-real-shortcode") == "something cached"
    end
  end
end
