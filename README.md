# Shorty

This project was setup to run locally with an actual installation of Elixir and Erlang or to use docker for running tasks.
For a list of available make commands you can run `make help`

## Overall decisions on the implementation

My overall approach was to make it as a MVP of the requested functionality.
With that in mind, there are some decisions that were deliberate but should be addressed in the process of refinement to consider it a final product.
I think more questionable one is the choice for the implementation of the shortcode, `:crypto.strong_rand_bytes/1` fallback to openssl `RAND_bytes` and on the long term this means a lot of conflicts with the generated shortcode.
There are a few ways to mitigate that but none of them are definitive, like defining a ttl for the shortcode.
Making the links unique too kinda helps on that mitigation, since it avoid using different shortcodes for the same url.

Besides that I changed all the initial structure of project to a way that I like better.
I'm really not a big fan of how Phoenix structure the initial code.
Given that to cover this exercise functionality there isn't required a lot of internal logic for the service, there is no much room to exercise the organization that much.

Althought I've created a cache for the shortcode, it could have a cache for the urls too.
I thought such optimization would be too early, the shortcode already cover the most common scenario to stress the database.
About the cache, I used ConCache to avoid making an early decision in case such a project go live, we had room to explore alternative caches.
I guess having Cachex setup would weight on using redis in the end.

About the design of the landing page and the usability of the site.
I've tried to be as simple as possible and doesn't spend too much time thinking on that.
I kept the phoenix logo just as a placeholder, the idea would be to substitute it for another image or just a heading of the site name.
I didn't work on anything of the design, most probably it would look awful anyway in the end.

I've tried to cover all edge cases with test either in the business rules layer and the web layer.
One edge case that the business rules doesn't cover is that the url might not exist, there could be GET request to the provided url to make sure it returns something in the 2XX range.

Althought the `Dockerfile` builds completely and the tests runs inside the container I think there would be more work required to have it fully running as a fully deployed system.
I didn't mess up with the `prod.exs` and `runtime.exs` config that much because all the changes there would depend on how we want to deploy that system.
The only thing that I made sure was that it could be deployed with releases and that we can run migrations on the deployed system.
For that you can check the script in `bin/start.sh`
I've also included the flags to turn off busy wait of the vm, since it can affects other systems running in the same server.
For more information on that you can read https://stressgrid.com/blog/beam_cpu_usage/
