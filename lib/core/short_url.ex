defmodule Core.ShortUrl do
  alias Core.Schemas.Url

  def find(shortcode) do
    case Cache.Shortcode.get(shortcode) do
      nil ->
        Url
        |> Core.Repo.get_by(shortcode: shortcode)
        |> maybe_cache()

      path ->
        path
    end
  end

  def create(params) do
    url = %{path: params["path"], shortcode: Core.Shortcode.generate()}

    with changeset <- Url.changeset(%Url{}, url),
         {:ok, url} <- Core.Repo.insert(changeset) do
      Cache.Shortcode.put(url.shortcode, url.path)
      url.shortcode
    else
      {:error, changeset} ->
        recreate_or_load(changeset, params)
    end
  end

  defp maybe_cache(nil), do: nil

  defp maybe_cache(url) do
    Cache.Shortcode.put(url.shortcode, url.path)

    url.path
  end

  defp recreate_or_load(changeset, params) do
    cond do
      changeset.errors[:path] ->
        changeset.errors[:path]
        |> classify_path_error()
        |> load_or_error(params)

      changeset.errors[:shortcode] ->
        create(params)

      true ->
        nil
    end
  end

  defp classify_path_error({"must be http or https", _}), do: :not_http
  defp classify_path_error({"must be a valid url", _}), do: :invalid_url
  defp classify_path_error({"has already been taken", _}), do: :exists

  defp load_or_error(error, _params) when error in ~w(not_http invalid_url)a, do: error

  defp load_or_error(:exists, params) do
    url = Core.Repo.get_by(Url, path: params["path"])
    maybe_cache(url)

    url.shortcode
  end
end
