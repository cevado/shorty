defmodule Core.Schemas.Url do
  use Core.Schema

  schema "urls" do
    field(:shortcode, :string)
    field(:path, :string)

    timestamps()
  end

  def changeset(struct, params) do
    struct
    |> Changeset.cast(params, ~w(shortcode path)a)
    |> Changeset.validate_required(~w(shortcode path)a)
    |> Changeset.unique_constraint(:shortcode)
    |> Changeset.unique_constraint(:path)
    |> Changeset.validate_change(:path, &is_http/2)
  end

  defp is_http(:path, path) do
    case URI.new(path) do
      {:ok, uri} when uri.scheme in ~w(http https) ->
        []

      {:ok, _uri} ->
        [path: "must be http or https"]

      _error ->
        [path: "must be a valid url"]
    end
  end
end
