defmodule Core.Shortcode do
  def generate do
    9
    |> :crypto.strong_rand_bytes()
    |> Base.url_encode64()
  end
end
