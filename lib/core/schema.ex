defmodule Core.Schema do
  defmacro __using__(_) do
    quote do
      use Ecto.Schema
      alias Core.Schemas
      alias Ecto.Changeset

      @primary_key {:id, :binary_id, autogenerate: true}
      @foreign_key_type :binary_id
      @typestamp_opts [type: :naive_datetime_usec]
    end
  end
end
