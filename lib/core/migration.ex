defmodule Core.Migration do
  @moduledoc """
  Release tasks, that should be used with release `eval`
  This runs migrations done with the Repo.
  """

  @app :shorty

  def run do
    for repo <- repos() do
      {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :up, all: true))
    end
  end

  defp repos do
    Application.load(@app)
    Application.fetch_env!(@app, :ecto_repos)
  end
end
