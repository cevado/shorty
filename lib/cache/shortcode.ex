defmodule Cache.Shortcode do
  @cache :shortcode_cache
  def config do
    {ConCache,
     [
       name: @cache,
       ttl_check_interval: :timer.minutes(1),
       touch_on_read: true,
       global_ttl: :timer.hours(1)
     ]}
  end

  def put(shortcode, path) do
    ConCache.put(@cache, shortcode, path)
  end

  def get(shortcode) do
    ConCache.get(@cache, shortcode)
  end
end
