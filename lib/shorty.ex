defmodule Shorty do
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      Cache.Shortcode.config(),
      Core.Repo,
      Web.Telemetry,
      Web.Endpoint
    ]

    opts = [strategy: :one_for_one, name: Shorty.Supervisor]
    Supervisor.start_link(children, opts)
  end

  @impl true
  def config_change(changed, _new, removed) do
    Web.Endpoint.config_change(changed, removed)
    :ok
  end
end
