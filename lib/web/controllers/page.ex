defmodule Web.Controllers.Page do
  use Web, :controller

  def index(conn, _params) do
    conn
    |> default_view()
    |> render("index.html")
  end

  def create(conn, params) do
    case Core.ShortUrl.create(params) do
      nil ->
        render_error(conn, "Something wrong is not right!")

      :not_http ->
        render_error(conn, "The url must include the http(s) part.")

      :invalid_url ->
        render_error(conn, "You must provide a valid url.")

      shortcode ->
        render_info(conn, "Copy this url: #{build_url(shortcode)}")
    end
  end

  def shortcode(conn, params) do
    case Core.ShortUrl.find(params["shortcode"]) do
      nil ->
        render_warning(conn, "Invalid short url!")

      path ->
        redirect(conn, external: path)
    end
  end

  defp render_info(conn, info) do
    conn
    |> default_view()
    |> put_flash(:info, info)
    |> put_status(:created)
    |> render("index.html")
  end

  defp render_error(conn, error) do
    conn
    |> default_view()
    |> put_flash(:error, error)
    |> put_status(:bad_request)
    |> render("index.html")
  end

  def render_warning(conn, warning) do
    conn
    |> default_view()
    |> put_flash(:warning, warning)
    |> put_status(:not_found)
    |> render("index.html")
  end

  defp default_view(conn) do
    conn
    |> put_layout({Views.Layout, "app.html"})
    |> put_view(Views.Page)
  end

  defp build_url(shortcode) do
    base_url = Web.Endpoint.url()
    Path.join(base_url, shortcode)
  end
end
