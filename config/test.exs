import Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :shorty, Core.Repo,
  username: "postgres",
  password: "postgres",
  hostname: System.get_env("SHORTY_DB_HOST") || "localhost",
  database: "shorty_test#{System.get_env("MIX_TEST_PARTITION")}",
  migration_primary_key: [type: :binary_id],
  migration_foreign_key: [type: :binary_id],
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :shorty, Web.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "XtopOOKYfkrYwyYFM98U6L8u4D+YoiRLPdxLzFRI01+JLxw130EAObEK0S5c/YkO",
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
